# Public Money Public Code CS

Czech translation of the brochure "[Public Money Public Code – Modernising Public Infrastructure with Free Software](https://fsfe.org/campaigns/publiccode/brochure)"
Contents of this report may be quoted or reproduced, provided that the source of information is acknowledged. All contents are, as long as not noted otherwise, licensed as [CC BY‐SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en).
For more details have a look at 2nd page of the [brochure](./pdf/FSFE_Policy_Brochure_CZ_RGB_200DPI_web.pdf).

Český překlad brožury "[Public Money Public Code – Modernising Public Infrastructure with Free Software](https://fsfe.org/campaigns/publiccode/brochure)"
Části této příručky mohou být citovány nebo reprodukovány za předpokladu uvedení zdroje. Všechny části příručky jsou chráněny licencí [CC BY‐SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.cs), není-li uvedeno jinak.
Podrobnosti naleznete na druhé stránce [brožury](/pdf/FSFE_Policy_Brochure_CZ_RGB_200DPI_web.pdf).